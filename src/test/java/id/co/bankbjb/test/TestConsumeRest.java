/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.co.bankbjb.test;

import org.json.JSONObject;
import org.springframework.web.client.RestTemplate;

/**
 *
 * @author LENOVO
 */
public class TestConsumeRest {

    public static void main(String[] args) throws InterruptedException {
//        createEmployee();
        System.out.println("Hello from main thread! - STARTING");

        new Thread(new TestNewThread()).start();
        
        System.out.println("Hello from main thread! - SLEEPING");
        Thread.sleep(5000);
        
        System.out.println("Hello from main thread! - FINISHED");
    }
    
    private static void getEmployees() {
        final String uri = "http://localhost:8080/springrestexample/employees";

        //TODO: Autowire the RestTemplate in all the examples
        RestTemplate restTemplate = new RestTemplate();

        String result = restTemplate.getForObject(uri, String.class);
        System.out.println(result);
    }

    private static void createEmployee() {
//        final String uri = "http://localhost:8080/springrestexample/employees";
        final String uri = "http://192.168.226.137:8888/valid_kesai_sid/";
        RestTemplate restTemplate = new RestTemplate();

//        EmployeeVO newEmployee = new EmployeeVO(-1, "Adam", "Gilly", "test@email.com");
        JSONObject newRequestObject = new JSONObject("{\"data\":[{\"extReff\":\"bjbtest20200921022331705\",\"isValid\":true,\"description\":\"SID and SRE valid\"}]}");

//        EmployeeVO result = restTemplate.postForObject(uri, newEmployee, EmployeeVO.class);
        JSONObject result = restTemplate.postForObject(uri, newRequestObject, JSONObject.class);

        System.out.println(result);
    }
}
