/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.co.bankbjb.test;

import org.json.JSONObject;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.web.client.RestTemplate;

/**
 *
 * @author LENOVO
 */
public class TestNewThread implements Runnable {

    @Override
    public void run() {
        System.out.println("Hello from a new thread! - STARTING");

        try {
            createEmployee();
        } catch (Exception e) {
            System.out.println("Hello from a new thread! - EXCEPTION");
            System.out.println("ERROR: " + e.getLocalizedMessage());
        }

        System.out.println("Hello from a new thread! - FINISHED");
    }

    private static void createEmployee() {
//        final String uri = "http://localhost:8080/springrestexample/employees";
        final String uri = "http://192.168.226.137:8888/valid_kesai_sid/";
        RestTemplate restTemplate = new RestTemplate();

//        EmployeeVO newEmployee = new EmployeeVO(-1, "Adam", "Gilly", "test@email.com");
        JSONObject newRequestObject = new JSONObject("{\"data\":[{\"extReff\":\"bjb20201206061926965\",\"isValid\":true,\"description\":\"SID and SRE valid\"}]}");

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        
        HttpEntity<String> request = new HttpEntity<>(newRequestObject.toString(), headers);
        
//        EmployeeVO result = restTemplate.postForObject(uri, newEmployee, EmployeeVO.class);
//        JSONObject result = restTemplate.postForObject(uri, newRequestObject, JSONObject.class);
        String result = restTemplate.postForObject(uri, request, String.class);

        System.out.println(result);
    }
}
