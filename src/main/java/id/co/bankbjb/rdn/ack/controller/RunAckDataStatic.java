/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.co.bankbjb.rdn.ack.controller;

import id.co.bankbjb.rdn.ack.config.YAMLConfig;
import id.co.bankbjb.rdn.ack.model.KseiRdnAccount;
import id.co.bankbjb.rdn.ack.repository.KseiRdnAccountRepository;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

/**
 *
 * @author LENOVO
 */
public class RunAckDataStatic implements Runnable {

    private static final Logger logger = LoggerFactory.getLogger(RunAckDataStatic.class);
    
    final String uri_dev = "http://192.168.226.137:8888/valid_data_static/";
    final String uri = "http://192.168.232.52:8888/valid_data_static/";

    @Autowired
    private YAMLConfig yAMLConfig;
    @Autowired
    private KseiRdnAccountRepository accountRepository;
    
    private JSONObject request = new JSONObject();
    private List<KseiRdnAccount> listAccount = new ArrayList<>();

    public RunAckDataStatic(JSONObject pRequest, List<KseiRdnAccount> listAccount) {
        this.request = pRequest;
        this.listAccount = listAccount;
    }

    @Override
    public void run() {
        logger.info("Starting ACK-DATA-STATIC REQUEST to validate new RDN");

        try {
            doAckDataStatic();
        } catch (Exception ex) {
            logger.error("ERROR SEND TO INTERNAL: " + ex.getMessage());
            logger.error("Ooops!", ex);
        }
        
    }

    private void doAckDataStatic() throws Exception {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        RestTemplate restTemplate = new RestTemplate();

        JSONObject jsonRequest = new JSONObject();

//        List<KseiRdnAccount> listAccountTemp = accountRepository.findAll();
//        logger.info("ARRAY DATA TEMP: " + new JSONArray(Arrays.asList(listAccountTemp)));
        
//        logger.info("BATCH-REFF: " + request.getString("batchReff"));
//        List<KseiRdnAccount> listAccount = accountRepository.findByDatastaticBatchReff(request.getString("batchReff"));
        
        JSONArray jsonArrayRequest = new JSONArray();
        JSONArray arrQuery = new JSONArray(listAccount);
        JSONArray arrRequest = request.getJSONArray("data");

        logger.info("QUERY DATA: " + arrQuery);
        
        for (int i = 0; i < arrRequest.length(); i++) {
            JSONObject arrElement = arrRequest.getJSONObject(i);
            JSONObject arrRequestElement = new JSONObject();

            Iterator<String> keys = arrElement.keys();
            StringBuilder sb = new StringBuilder();

            while (keys.hasNext()) {
                String key = keys.next();
                if (arrElement.get(key).equals("NotValid")) {
                    sb.append(key).append(", ");
                }
            }

            sb.append("is NotValid");

            arrRequestElement.put("extReff", arrElement.getString("relatedReferenceData"));
            arrRequestElement.put("isValid", false);
            arrRequestElement.put("description", sb.toString());

            for (int j = 0; j < arrQuery.length(); j++) {
                JSONObject accountObj = arrQuery.getJSONObject(i);
                
                if (accountObj.getString("datastaticReff").equals(arrElement.getString("relatedReferenceData"))) {
                    jsonArrayRequest.put(arrRequestElement);
                } else {
                    JSONObject accountReq = new JSONObject();
                    
                    accountReq.put("extReff", accountObj.getString("datastaticReff"));
                    accountReq.put("isValid", true);
                    accountReq.put("description", "VALID");
                    
                    jsonArrayRequest.put(accountReq);
                }
            }
        }
        
        jsonRequest.put("batchReff", request.getString("batchReff"));
        jsonRequest.put("data", jsonArrayRequest);
        
        try {
            logger.info("REQ-DATASTATIC-VALID: " + jsonRequest);
//            logger.info("URL: " + yAMLConfig.getTestconfig());
            
            HttpEntity<String> requestStr = new HttpEntity<>(jsonRequest.toString(), headers);
            
            String result = restTemplate.postForObject(uri_dev, requestStr, String.class);
            
            logger.info("RSP-DATASTATIC-VALID: " + result);
        } catch (RestClientException ex) {
            logger.error("ERROR: " + ex.getMessage());
        } catch (Exception ex) {
            logger.error("UNKNOWN_ERROR: " + ex.getMessage());
        }
    }

}
