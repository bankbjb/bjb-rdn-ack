package id.co.bankbjb.rdn.ack;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.scheduling.annotation.EnableAsync;

@SpringBootApplication
public class BjbRdnAckApplication extends SpringBootServletInitializer {

    public static void main(String[] args) {
        SpringApplication.run(BjbRdnAckApplication.class, args);
    }
}
