package id.co.bankbjb.rdn.ack.controller;

import id.co.bankbjb.rdn.ack.model.KseiRdnAccount;
import id.co.bankbjb.rdn.ack.repository.KseiRdnAccountRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.mvc.method.annotation.StreamingResponseBody;

import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.util.Arrays;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.soap.MessageFactory;
import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPConstants;
import javax.xml.soap.SOAPEnvelope;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;
import javax.xml.soap.SOAPPart;
import javax.xml.transform.stream.StreamSource;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.XML;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.Base64Utils;
import org.springframework.web.bind.annotation.PostMapping;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

@RestController
@RequestMapping("/rdn")
public class AckController {

    private static final Logger logger = LoggerFactory.getLogger(AckController.class);
    private static final int INDENTATION = 4;
    
    @Autowired
    private KseiRdnAccountRepository accountRepository;

    @PostMapping(value = "/ack.wsdl")
    public String receiveAck(HttpServletRequest request) {
        byte messageByte;
        byte endMessageByte = -0x01;

        String responseSuccess = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><soap:Envelope xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\"><soap:Header/><soap:Body><ReceiveAckResponse xmlns=\"http://tempuri.org/\"><ReceiveAckResult>1</ReceiveAckResult></ReceiveAckResponse></soap:Body></soap:Envelope>";
        String responseFailed = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><soap:Envelope xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\"><soap:Header/><soap:Body><ReceiveAckResponse xmlns=\"http://tempuri.org/\"><ReceiveAckResult>0</ReceiveAckResult></ReceiveAckResponse></soap:Body></soap:Envelope>";

        StringBuilder sb = new StringBuilder();
        String requestStream = null;

        logger.info("READY TO RECEIVE INCOMING ACK");

        try {
            while ((messageByte = (byte) request.getInputStream().read()) != -1) {
                sb.append((char) messageByte);

                requestStream = sb.toString();
            }

            logger.info("ACK REQUEST: " + requestStream);
        } catch (IOException ex) {
            logger.error(ex.getMessage());

            logger.info(responseFailed);
            return responseFailed;
        }

        SOAPMessage sampleMessage = createRequest(requestStream);
        SOAPBody soapBody = null;

        try {
            soapBody = sampleMessage.getSOAPBody();
        } catch (SOAPException ex) {
            logger.error(ex.getMessage());

            logger.info(responseFailed);
            return responseFailed;
        }

        NodeList nodesMemberCode = soapBody.getElementsByTagName("memberCode");
        NodeList nodesFileContents = soapBody.getElementsByTagName("fileContents");

        //check if the node exists and get the value
        String fileContentsValue = null;
        Node nodeFileContents = nodesFileContents.item(0);
        fileContentsValue = nodeFileContents != null ? nodeFileContents.getTextContent() : "";

        String fileContentsValueDecoded = new String(Base64Utils.decodeFromString(fileContentsValue));

        logger.info("FILE_CONTENTS_ENCODED: " + fileContentsValue);
//        logger.info("FILE_CONTENTS_DECODED: " + new String(Base64Utils.decodeFromString(fileContentsValue)));
        logger.info("FILE_CONTENTS_DECODED: " + fileContentsValueDecoded);

        Document xmlFileContents = convertStringToXMLDocument(fileContentsValueDecoded);

        JSONObject jsonRequest = new JSONObject();
        JSONArray dataArray = new JSONArray();

        //Normalize the XML Structure; It's just too important !!
        xmlFileContents.getDocumentElement().normalize();

        //Here comes the root node
        Element root = xmlFileContents.getDocumentElement();

        //Get all messages
        NodeList nodeMessage = xmlFileContents.getElementsByTagName("Message");
        NodeList nodeList = xmlFileContents.getElementsByTagName("List");

        jsonRequest.put("data", visitChildNodesListNonRecursive(nodeList));

        logger.info("LIST FORMATTED: " + jsonRequest);

        if (nodeMessage.item(0).getAttributes().item(0).getNodeValue().equals("AckDataInvestorValidation")) {
            new Thread(new RunAckValidation(jsonRequest)).start();
        } else if (nodeMessage.item(0).getAttributes().item(0).getNodeValue().equals("AckDataStaticInvestor")) {
            jsonRequest.put("batchReff", visitChildNodesRelatedBatchReference(nodeMessage));
            
            List<KseiRdnAccount> listAccount = accountRepository.findByDatastaticBatchReff(jsonRequest.getString("batchReff"));
            
            new Thread(new RunAckDataStatic(jsonRequest, listAccount)).start();
        }

//        visitChildNodes(nList);
//        JSONObject jsonMessage = new JSONObject();
//        jsonMessage.put("message", nodeMessage.item(0).getAttributes().item(0).getNodeValue());
//        NodeList nListRecord = nodeMessage.item(0).getChildNodes();
//        NodeList nFields = nListRecord.item(0).getChildNodes();
//        JSONObject jsonRecord = visitChildNodesNonRecursive(nFields);
//        jsonRequest = mergeJSONObjects(jsonMessage, jsonRecord);
//        NodeList nListDetail = nFields.
//        JSONObject jsonRecordDetail = 
//        jsonMessage.put("message", visitChildNodes(nodeMessage));
//        logger.info("JSON-FORMATTED: " + jsonMessage);
        //https://www.techiedelight.com/convert-xml-to-json-in-java/
        JSONObject jsonFileContents = XML.toJSONObject(fileContentsValueDecoded);
        String json = jsonFileContents.toString(INDENTATION);

        logger.info("JSON-FILE_CONTENTS: " + jsonFileContents);
//        logger.info("JSON-FILE_CONTENTS: " + json);

        logger.info(responseSuccess);
        logger.info("ACK RESPONDED SUCCESSFULLY");

        return responseSuccess;
    }

    private static SOAPMessage createRequest(String msg) {
        SOAPMessage request = null;

        try {
            MessageFactory msgFactory = MessageFactory.newInstance(SOAPConstants.SOAP_1_1_PROTOCOL);
            request = msgFactory.createMessage();

            SOAPPart msgPart = request.getSOAPPart();
            SOAPEnvelope envelope = msgPart.getEnvelope();
            SOAPBody body = envelope.getBody();
//            System.out.println("SOAPMessage Create Part: " + msgPart.getElementsByTagName("ReceiveAck"));
//            System.out.println("SOAPMessage Create Envelope: " + envelope);
//            System.out.println("SOAPMessage Create Body: " + body);

            StreamSource _msg = new StreamSource(new StringReader(msg));
            msgPart.setContent(_msg);

            request.saveChanges();
        } catch (SOAPException ex) {
            logger.error(ex.getMessage());
        }

        return request;
    }

    private static Document convertStringToXMLDocument(String xmlString) {
        //Parser that produces DOM object trees from XML content
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();

        //API to obtain DOM Document instance
        DocumentBuilder builder = null;
        try {
            //Create DocumentBuilder with default configuration
            builder = factory.newDocumentBuilder();

            //Parse the content to Document object
            Document doc = builder.parse(new InputSource(new StringReader(xmlString)));
            return doc;
        } catch (IOException | ParserConfigurationException | SAXException ex) {
            logger.error(ex.getMessage());
        }
        return null;
    }

    private static JSONArray visitChildNodes(NodeList nList) {
        JSONArray arrayResult = new JSONArray();

        for (int temp = 0; temp < nList.getLength(); temp++) {
            JSONObject jsonResult = new JSONObject();

            Node node = nList.item(temp);

            if (node.getNodeType() == Node.ELEMENT_NODE) {
                logger.trace("Node Name = " + node.getNodeName() + "; Value = " + node.getTextContent());

                //Check all attributes
                if (node.hasAttributes()) {
                    // get attributes names and values
                    NamedNodeMap nodeMap = node.getAttributes();

                    String nodeName = node.getNodeName() + temp;

                    for (int i = 0; i < nodeMap.getLength(); i++) {
                        Node tempNode = nodeMap.item(i);

                        logger.trace("Attr name : " + tempNode.getNodeName() + "; Value = " + tempNode.getNodeValue());

                        if (tempNode.getNodeName().equals("name")) {
                            jsonResult.put(tempNode.getNodeValue(), node.getTextContent());

                            arrayResult.put(jsonResult);
                            nodeName = node.getTextContent();

                            logger.info("JSON-AAA: " + jsonResult);
                        }
                    }

                    if (node.hasChildNodes()) {
                        //We got more childs; Let's visit them as well
                        JSONArray arrayChild = visitChildNodes(node.getChildNodes());

                        jsonResult.put(nodeName, arrayChild);
                        arrayResult.put(jsonResult);

                        logger.info("JSON-BBB: " + arrayChild);
                    }
                }
            }
        }

        return arrayResult;
    }

    private static JSONArray visitChildNodesListNonRecursive(NodeList nodeList) {
        JSONArray arrayResult = new JSONArray();

        Node nodeParent = nodeList.item(0);
        NodeList nList = nodeParent.getChildNodes();

        for (int temp = 0; temp < nList.getLength(); temp++) {
//            JSONObject jsonResult;

            Node node = nList.item(temp);

            if (node.getNodeType() == Node.ELEMENT_NODE) {
                logger.trace("Node Name = " + node.getNodeName() + "; Value = " + node.getTextContent());

                //Check all attributes
                if (node.hasAttributes()) {
                    // get attributes names and values
                    NamedNodeMap nodeMap = node.getAttributes();

                    String nodeName = node.getNodeName() + temp;

                    for (int i = 0; i < nodeMap.getLength(); i++) {
                        Node tempNode = nodeMap.item(i);

                        logger.trace("Attr name : " + tempNode.getNodeName() + "; Value = " + tempNode.getNodeValue());

                        if (tempNode.getNodeName().equals("name") && tempNode.getNodeValue().equals("detailData")) {
//                            jsonResult.put(tempNode.getNodeValue(), node.getTextContent());

//                            arrayResult.put(jsonResult);
//                            nodeName = node.getTextContent();
//                            logger.info("JSON-AAA: " + jsonResult);
                            JSONObject jsonResult = new JSONObject();
                            NodeList nodeChilds = node.getChildNodes();
//                            logger.trace("LENGTH: " + nodeChilds.getLength());
                            for (int tempChild = 0; tempChild < nodeChilds.getLength(); tempChild++) {
                                Node nodeChild = nodeChilds.item(tempChild);

                                if (nodeChild.getNodeType() == Node.ELEMENT_NODE) {
                                    if (nodeChild.hasAttributes()) {
                                        NamedNodeMap nodeChildMap = nodeChild.getAttributes();

                                        for (int j = 0; j < nodeChildMap.getLength(); j++) {
                                            Node tempChildNode = nodeChildMap.item(j);

                                            if (tempChildNode.getNodeName().equals("name")) {
                                                jsonResult.put(tempChildNode.getNodeValue(), nodeChild.getTextContent());

//                                                logger.trace("ADD TO DATA ELEMENT_" + temp + ": " + jsonResult);
                                            }
                                        }
                                    }
                                }
                            }

//                            logger.trace("ADD TO LIST DATA: " + jsonResult);
                            arrayResult.put(jsonResult);
                        }
                    }

//                    if (node.hasChildNodes()) {
                    //We got more childs; Let's visit them as well
//                        JSONArray arrayChild = visitChildNodes(node.getChildNodes());
//                        jsonResult.put(nodeName, arrayChild);
//                        arrayResult.put(jsonResult);
//                        logger.info("JSON-BBB: " + arrayChild);
//                    }
                }
            }
        }

        return arrayResult;
    }

    private static String visitChildNodesRelatedBatchReference(NodeList nodeList) {
        String result = "";

        Node nodeParent = nodeList.item(0);
        NodeList nList = nodeParent.getChildNodes();

        for (int temp = 0; temp < nList.getLength(); temp++) {
//            JSONObject jsonResult;

            Node node = nList.item(temp);

            if (node.getNodeType() == Node.ELEMENT_NODE) {
                logger.trace("Node Name = " + node.getNodeName() + "; Value = " + node.getTextContent());

                //Check all attributes
                if (node.hasAttributes()) {
                    // get attributes names and values
                    NamedNodeMap nodeMap = node.getAttributes();

                    String nodeName = node.getNodeName() + temp;

                    for (int i = 0; i < nodeMap.getLength(); i++) {
                        Node tempNode = nodeMap.item(i);

                        logger.trace("Attr name : " + tempNode.getNodeName() + "; Value = " + tempNode.getNodeValue());

                        if (tempNode.getNodeName().equals("name") && tempNode.getNodeValue().equals("dataStaticInvestor")) {
//                            jsonResult.put(tempNode.getNodeValue(), node.getTextContent());

//                            arrayResult.put(jsonResult);
//                            nodeName = node.getTextContent();
//                            logger.info("JSON-AAA: " + jsonResult);
                            JSONObject jsonResult = new JSONObject();
                            NodeList nodeChilds = node.getChildNodes();
//                            logger.trace("LENGTH: " + nodeChilds.getLength());
                            for (int tempChild = 0; tempChild < nodeChilds.getLength(); tempChild++) {
                                Node nodeChild = nodeChilds.item(tempChild);

                                if (nodeChild.getNodeType() == Node.ELEMENT_NODE) {
                                    if (nodeChild.hasAttributes()) {
                                        NamedNodeMap nodeChildMap = nodeChild.getAttributes();

                                        for (int j = 0; j < nodeChildMap.getLength(); j++) {
                                            Node tempChildNode = nodeChildMap.item(j);

                                            if (tempChildNode.getNodeName().equals("name") && tempChildNode.getNodeValue().equals("relatedBatchReference")) {
                                                jsonResult.put(tempChildNode.getNodeValue(), nodeChild.getTextContent());

                                                logger.trace("ADD TO DATA ELEMENT_BATCHREFF" + temp + ": " + jsonResult);
                                                
                                                result = nodeChild.getTextContent();
                                            }
                                        }
                                    }
                                }
                            }

//                            logger.trace("ADD TO LIST DATA: " + jsonResult);
//                            result.put(jsonResult);
                        }
                    }

//                    if (node.hasChildNodes()) {
                    //We got more childs; Let's visit them as well
//                        JSONArray arrayChild = visitChildNodes(node.getChildNodes());
//                        jsonResult.put(nodeName, arrayChild);
//                        arrayResult.put(jsonResult);
//                        logger.info("JSON-BBB: " + arrayChild);
//                    }
                }
            }
        }

        return result;
    }
    
    //This function is called recursively
    private JSONObject visitChildNodesNonRecursive(NodeList nList) {
        JSONObject results = new JSONObject();

        for (int temp = 0; temp < nList.getLength(); temp++) {
            Node node = nList.item(temp);

            if (node.getNodeType() == Node.ELEMENT_NODE) {
                logger.trace("Node Name = " + node.getNodeName() + "; Value = " + node.getTextContent());

                //Check all attributes
                if (node.hasAttributes()) {
                    // get attributes names and values
                    NamedNodeMap nodeMap = node.getAttributes();

                    for (int i = 0; i < nodeMap.getLength(); i++) {
                        Node tempNode = nodeMap.item(i);

                        logger.trace("Attr name : " + tempNode.getNodeName() + "; Value = " + tempNode.getNodeValue());

                        if (tempNode.getNodeName().equals("name")) {
                            results.put(tempNode.getNodeValue(), node.getTextContent());
                        }
                    }
                } else {
                    results.put(node.getNodeName() + temp, node.getTextContent());
                }
            }
        }

        return results;
    }

    public static JSONObject mergeJSONObjects(JSONObject json1, JSONObject json2) {
        JSONObject mergedJSON = new JSONObject();
        try {
            mergedJSON = new JSONObject(json1, JSONObject.getNames(json1));
            for (String crunchifyKey : JSONObject.getNames(json2)) {
                mergedJSON.put(crunchifyKey, json2.get(crunchifyKey));
            }

        } catch (JSONException ex) {
            throw new RuntimeException("JSON Exception" + ex);
        }
        return mergedJSON;
    }
}
