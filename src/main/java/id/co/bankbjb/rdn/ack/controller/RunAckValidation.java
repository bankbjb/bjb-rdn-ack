/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.co.bankbjb.rdn.ack.controller;

import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

/**
 *
 * @author LENOVO
 */
public class RunAckValidation implements Runnable {

    private static final Logger logger = LoggerFactory.getLogger(RunAckValidation.class);

    final String uri_dev = "http://192.168.226.137:8888/valid_kesai_sid/";
    final String uri = "http://192.168.232.52:8888/valid_kesai_sid/";

    private JSONObject request = new JSONObject();

    public RunAckValidation(JSONObject pRequest) {
        this.request = pRequest;
    }

    @Override
    public void run() {
        logger.info("Starting ACK-VALIDATION REQUEST to create new RDN");

        doAckValidation();
    }

    private void doAckValidation() {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        RestTemplate restTemplate = new RestTemplate();

        JSONObject jsonRequest = new JSONObject();
        
        JSONArray jsonArrayRequest = new JSONArray();
        JSONArray arrRequest = request.getJSONArray("data");
        
        for (int i = 0; i < arrRequest.length(); i++) {
            JSONObject arrElement = arrRequest.getJSONObject(i);
            JSONObject arrRequestElement = new JSONObject();

            if (arrElement.getString("sidNumberData").equals("Valid") && arrElement.getString("accountNumberData").equals("Valid")) {
                arrRequestElement.put("extReff", arrElement.getString("relatedReferenceData"));
                arrRequestElement.put("isValid", true);
                arrRequestElement.put("description", "SID and SRE valid");
            } else {
                arrRequestElement.put("extReff", arrElement.getString("relatedReferenceData"));
                arrRequestElement.put("isValid", false);
                arrRequestElement.put("description", "SID and SRE invalid");
            }

            jsonArrayRequest.put(arrRequestElement);
        }

        jsonRequest.put("data", jsonArrayRequest);

        try {
            logger.info("REQ-ACK-VALID: " + jsonRequest);

            HttpEntity<String> requestStr = new HttpEntity<>(jsonRequest.toString(), headers);

            String result = restTemplate.postForObject(uri_dev, requestStr, String.class);

            logger.info("RSP-ACK-VALID: " + result);
        } catch (RestClientException ex) {
            logger.error("ERROR: " + ex.getMessage());
        }

    }
}
