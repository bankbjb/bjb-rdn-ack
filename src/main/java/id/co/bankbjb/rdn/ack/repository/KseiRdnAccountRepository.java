/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.co.bankbjb.rdn.ack.repository;

import id.co.bankbjb.rdn.ack.model.KseiRdnAccount;
import id.co.bankbjb.rdn.ack.model.KseiRdnAccountId;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author LENOVO
 */
@Repository
public interface KseiRdnAccountRepository extends JpaRepository<KseiRdnAccount, KseiRdnAccountId> {
    
    List<KseiRdnAccount> findByDatastaticBatchReff(String datastaticBatchReff);
}
