/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.co.bankbjb.rdn.ack.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;
import org.hibernate.annotations.DynamicUpdate;

/**
 *
 * @author LENOVO
 */
@Entity
@DynamicUpdate
@IdClass(KseiRdnAccountId.class)
@Table(name = "ksei_rdn_account")
public class KseiRdnAccount {
    
    @Id
    @Column(name = "sid")
    private String sid;
    
    @Id
    @Column(name = "sre")
    private String sre;
    
    @Column(name = "rdn")
    private String rdn;
    
    @Column(name = "extreff_validation")
    private String validationReff;
    
    @Column(name = "ack_validation")
    private String validationAck;
    
    @Column(name = "batchreff_datastatic")
    private String datastaticBatchReff;
    
    @Column(name = "extreff_datastatic")
    private String datastaticReff;
    
    @Column(name = "ack_datastatic")
    private String dataStaticAck;

    public KseiRdnAccount() {
    }

    public String getSid() {
        return sid;
    }

    public void setSid(String sid) {
        this.sid = sid;
    }

    public String getSre() {
        return sre;
    }

    public void setSre(String sre) {
        this.sre = sre;
    }

    public String getRdn() {
        return rdn;
    }

    public void setRdn(String rdn) {
        this.rdn = rdn;
    }

    public String getValidationReff() {
        return validationReff;
    }

    public void setValidationReff(String validationReff) {
        this.validationReff = validationReff;
    }

    public String getValidationAck() {
        return validationAck;
    }

    public void setValidationAck(String validationAck) {
        this.validationAck = validationAck;
    }

    public String getDatastaticBatchReff() {
        return datastaticBatchReff;
    }

    public void setDatastaticBatchReff(String datastaticBatchReff) {
        this.datastaticBatchReff = datastaticBatchReff;
    }

    public String getDatastaticReff() {
        return datastaticReff;
    }

    public void setDatastaticReff(String datastaticReff) {
        this.datastaticReff = datastaticReff;
    }

    public String getDataStaticAck() {
        return dataStaticAck;
    }

    public void setDataStaticAck(String dataStaticAck) {
        this.dataStaticAck = dataStaticAck;
    }
    
}
