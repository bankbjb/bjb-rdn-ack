/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.co.bankbjb.rdn.ack.model;

import java.io.Serializable;
import java.util.Objects;

/**
 *
 * @author LENOVO
 */
public class KseiRdnAccountId implements Serializable {
    
    private String sid;
    private String sre;

    public KseiRdnAccountId() {
    }

    public KseiRdnAccountId(String sid, String sre) {
        this.sid = sid;
        this.sre = sre;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 67 * hash + Objects.hashCode(this.sid);
        hash = 67 * hash + Objects.hashCode(this.sre);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final KseiRdnAccountId other = (KseiRdnAccountId) obj;
        if (!Objects.equals(this.sid, other.sid)) {
            return false;
        }
        if (!Objects.equals(this.sre, other.sre)) {
            return false;
        }
        return true;
    }
    
}
